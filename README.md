# Introduction

Le service Nginx peut gérer plusieurs sites web sur la même machine.

Nous allons illustrer cette possibilité durant cette activité.

# Environnement

Système d'exploitation: dernière version de linux Debian stable (>=9)

Matériel: Raspberry pi (>= 3b)

La raspberry doit être connecté à un réseau local fonctionnel 

Comme nous allons assigner manuellement de nouvelles adresses ip dans la suite de cette activité,
vous devrez vous assurer au préalable qu'elles ne sont pas déjà prises et que vous avez le droit de les utiliser
(demandez éventuellement à votre administrateur réseau un lot d'adresse ip dites statiques qu'il peut vous autoriser à utiliser temporairement)


# Avertissement

* les paramètres indiqués dans la suite de cette activité sont fournis à titre d'exemple.

L'adresse ip 192.168.1.101 est par exemple à adapter à votre contexte.

Si vous êtes plusieurs à réaliser cette activité sur le même réseau local, assurez-vous de vous coordonner pour qu'il n'y ait pas la même adresse ip assignée à plusieurs serveurs.

Si cela se produit, votre site ne fonctionnera pas.

* Il est possible que certaines commandes nécessitent des droits super-utilisateur.

Dans ce cas, mettez-vous au préalable dans un contexte de super-utilisateur via la commande:
`sudo -i`



# Paquets à installer au préalable

`sudo apt install -y nginx`

# Etapes

## Lister les interfaces réseau, les addresses ip et mac

la commande:
`ip addr`

vous affiche la liste des interfaces réseaux détectées ainsi que leurs paramètres.

Pour voir l'ip d'une interface en particulier:
`ip addr show dev MON_INTERFACE`

Exemple:
`ip addr show dev eth0`

**Travail à réaliser:** 

Exécutez les commandes ci-dessus, observer les résultats associées et essayer de comprendre la signification des différentes champs.

Quelles sont les addresses MAC associées à chaque équipement?

Quel est le masque réseau de chaque adresse ip ?

Quel est le nom de l'interface ethernet?

Il y a t'il une interface wifi ?

Quel est le nom et l'addresse ip de l'interface locale du serveur ?

## Assignation de plusieurs ip sur votre serveur

### vérification de l'état actuel

Choisissez une adresse ip existante de votre serveur qui soit accessible depuis une autre machine

Nous prendrons pour exemple dans la suite de cette activité:
ip = 192.168.1.100
interface = eth0


Depuis une autre machine, accédez à la page web:
  http://192.168.1.100

Assurez-vous que cette page web est accissible et qu'une page par défaut s'affiche

(elle doit correspondre au fichier /var/www/html/index.nginx-debian.html stocké sur votre serveur)

### assignation d'une 2ème adresse ip

assignez une 2ème adresse ip fixe de votre choix sur l'interface réseau publique de votre serveur.

La syntaxe est la suivante:
`sudo ip addr add MA_NOUVELLE_ADRESSE_IP dev NOM_DE_MON_INTERFACE_RESEAU`

Exemple:
`ip addr add 192.168.1.101 dev  eth0`

Vous devriez ensuite voir apparaitre 2 addresses ip associées à la même interface via la commande:
`ip addr show`


## Configuration d'nginx pour servir deux sites différents

# Etapes

## création de deux sites web sur votre serveur

Créez une page index.html avec un contenu différent dans les deux répertoires suivants:
`/var/www/site1/index.html`
`/var/www/site2/index.html`

déplacez le fichier:
`/etc/nginx/sites-available/default` en
`/etc/nginx/sites-available/site1`

et dupliquez le fichier site1 en site2

editez les fichiers site1 et site2 et apportez les modifications suivantes:
supprimez le paramètre default_server des directives listen

adaptez le paramètre de la directive:
`root` des fichiers site1 et site2 pour qu'ils pointent vers leur bon répertoire respectif sur le serveur.

Allez dans:
`/etc/nginx/sites-enabled`

Listez les fichiers:
`ls -al `

Supprimez le lien symbolique default qui pointe maintenant vers un fichier inexistant

créez deux nouveaux liens symboliques pointant vers les sites disponibles site1 et site2 que vous venez de configurer.

La création d'un lien symbolique s'effectue via la commande:
`ln -snf DESTINATION SOURCE`

exemple:
`ln -snf /etc/nginx/sites-available/toto1 /etc/nginx/sites-enabled/toto1`

Un lien symbolique est en quelque sorte un raccourci de fichier/dossier.

Notifiez nginx que la configuration a changé:
`sudo systemctl reload nginx.service`


Vérifiez depuis une machine distante que vous visualisez des pages web différentes pour chacune des adresses ip assignées sur votre serveur.


**Questions**
Pourquoi avons-nous supprimé default_server des directives listen? Que se passe t'il si nous ne le faisons pas?



## Simulation de noms de domaine

Dans les étapes précédentes, nous avons toujours indiqué pour le paramètre `servername` d'nginx des addresses ip.

Pour des sites officiels, vous utiliserez probablement plutôt des noms de domaines pointant vers des addresses ip.

Le cas échéant, il faudra juste indiquer le nom de domaine comme valeur du paramètre `servername`  d'nginx à la place de l'adresse ip.

Vous avez la possibilité de simuler l'achat de noms de domaine via les services du site nip.io
pour mettre en pratique l'usage de noms de domaine dans nginx.

En écrivant une URL sous la forme: htt://MON_IP.nip.io
le site nip.io vous redirigera automatique vers l'adresse ip MON_IP

**Travail à réaliser:** 
Modifiez la configuration de vos deux sites d'nginx pour qu'ils servent deux noms de domaines différents.
